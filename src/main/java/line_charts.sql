SELECT         
        c.periodo as periodo,
        c.ano AS ano,
        c.mes AS mes,
        SUM(c.entrada) AS entrada,
        SUM(c.saida) AS saida
    FROM
        (SELECT            
                CONCAT(CASE MONTH(a.data)
					 WHEN 1 THEN 'JAN'
						WHEN 2 THEN 'FEV'
						WHEN 3 THEN 'MAR'
						WHEN 4 THEN 'ABR'
						WHEN 5 THEN 'MAI'
						WHEN 6 THEN 'JUN'
						WHEN 7 THEN 'JUL'
						WHEN 8 THEN 'AGO'
						WHEN 9 THEN 'SET'
						WHEN 10 THEN 'OUT'
						WHEN 11 THEN 'NOV'
						ELSE 'DEZ' 
				END, '/', YEAR(a.data)) as periodo,                
                YEAR(a.data) AS ano,
                LPAD(MONTH(a.data), 2, 0) AS mes,
                SUM(a.valor) AS entrada,
                0 AS saida
        FROM
            fluxo a
        WHERE
            (a.valor > 0)
        GROUP BY YEAR(a.data) , MONTH(a.data) UNION SELECT            
				CONCAT(CASE MONTH(a.data)
					 WHEN 1 THEN 'JAN'
						WHEN 2 THEN 'FEV'
						WHEN 3 THEN 'MAR'
						WHEN 4 THEN 'ABR'
						WHEN 5 THEN 'MAI'
						WHEN 6 THEN 'JUN'
						WHEN 7 THEN 'JUL'
						WHEN 8 THEN 'AGO'
						WHEN 9 THEN 'SET'
						WHEN 10 THEN 'OUT'
						WHEN 11 THEN 'NOV'
						ELSE 'DEZ' 
				END, '/', YEAR(a.data)) as periodo,                
                YEAR(a.data) AS ano,
                LPAD(MONTH(a.data), 2, 0) AS mes,
                0 AS entrada,
                (SUM(a.valor) * -(1)) AS saida
        FROM
            fluxo a
        WHERE
            (a.valor < 0)
        GROUP BY YEAR(a.data) , MONTH(a.data)) c       
    GROUP BY c.ano , c.mes;