package br.com.prj.converters;

import java.math.BigDecimal;

import org.springframework.core.convert.converter.Converter;

public class BigDecimalConverter implements Converter<String, BigDecimal> {

	@Override
	public BigDecimal convert(String source) {
		try {
			source = source.replaceAll("[R$ .]", "").replace(",", ".");
			return new BigDecimal(source);
		} catch (Exception e) {
			return null;
		}
	}

}
