package br.com.prj.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

public class CalendarConverter implements Converter<String, Calendar> {

	private static final Logger LOG = LoggerFactory.getLogger(CalendarConverter.class);
	
	@Override
	public Calendar convert(String source) {
		
		if(StringUtils.isEmpty(source)) { 
			return null;
		}

		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(source);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException e) {
			LOG.error("Invalid string to be parse to date (dd/MM/yyyy) {}", source);
			return null;
		}
		
	}
	
}
