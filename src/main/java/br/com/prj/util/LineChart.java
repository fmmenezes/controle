package br.com.prj.util;

public class LineChart {

	public LineChart(String periodo, String receita, String despesa) {
		this.periodo = periodo;
		this.receita = receita;
		this.despesa = despesa;
	}

	private String periodo;
	private String receita;
	private String despesa;

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getReceita() {
		return receita;
	}

	public void setReceita(String receita) {
		this.receita = receita;
	}

	public String getDespesa() {
		return despesa;
	}

	public void setDespesa(String despesa) {
		this.despesa = despesa;
	}

}
