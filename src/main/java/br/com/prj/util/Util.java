package br.com.prj.util;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class Util {

	public final static String WARN = "alert-warning";
	public final static String INFO = "alert-info";
	public final static String SUCCESS = "alert-success";
	public final static String ERROR = "alert-danger";

	public static boolean onlyNumbers(String s) {
		try {
			Long.parseLong(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static boolean isNull(String s) {
		return (s == null) || (s.trim().equals("")) || (s.trim().length() == 0);
	}

	public static boolean containsNull(String... strings) {
		for (String s : strings) {
			if (isNull(s)) {
				return true;
			}
		}
		return false;
	}

	public static void setMessage(RedirectAttributes attr, String message, String type) {
		attr.addFlashAttribute("message", message);
		attr.addFlashAttribute("alert", type);
	}
	
	public static void setSuccessMessage(RedirectAttributes attr) {
		attr.addFlashAttribute("message", "Operação realizada com sucesso");
		attr.addFlashAttribute("alert", SUCCESS);
	}
	
	public static void setErrorMessage(RedirectAttributes attr) {
		attr.addFlashAttribute("message", "Houve uma falha ao realizar essa operação");
		attr.addFlashAttribute("alert", ERROR);
	}
}