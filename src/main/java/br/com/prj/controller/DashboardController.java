package br.com.prj.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.prj.services.FluxoService;
import br.com.prj.util.LineChart;

@Controller
public class DashboardController {

	@Autowired
	private FluxoService fluxoService;

	@RequestMapping(value = "lineCharts")
	public @ResponseBody List<LineChart> getLineCharts() throws IOException {
		return fluxoService.getLineCharts();
	}

	@RequestMapping(value = "/")
	public ModelAndView home() {
		return new ModelAndView("dashboard/index");
	}

}
