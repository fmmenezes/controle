package br.com.prj.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

	@RequestMapping("/favicon.ico")
    public String favicon() {
		System.err.println("-- FAVICON ---");
		return "forward:/resources/app/images/favicon.ico";
    }

}