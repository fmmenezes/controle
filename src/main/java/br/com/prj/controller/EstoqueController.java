package br.com.prj.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.prj.models.Estoque;
import br.com.prj.services.EstoqueService;
import br.com.prj.util.Util;

@Controller
@RequestMapping("/estoque")
public class EstoqueController {

	@Autowired
	private EstoqueService estoqueService;

	@RequestMapping
	public ModelAndView list(
			@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(value = "descricao", required = false) String descricao) {
		
		ModelAndView modelAndView = new ModelAndView("estoque/index");
		
		PageRequest pageable = new PageRequest(page - 1, 5);
		
		if (Util.isNull(descricao)) {
			modelAndView.addObject("list", estoqueService.findAllByOrderByDescricao(pageable));
		} else {
			modelAndView.addObject("list", estoqueService.findAllByDescricaoContainingOrderByDescricao(descricao, pageable));
		}
		
		return modelAndView;
	}
	

	@RequestMapping(value = { "/new", "/edit" })
	public ModelAndView view(@RequestParam(value = "id", required = false) Long id) {
		ModelAndView modelAndView = new ModelAndView("estoque/modal");
		
		if(id == null) {
			modelAndView.addObject("estoque", new Estoque());
		} else {
			modelAndView.addObject("estoque", estoqueService.findById(id));
		}
		
		return modelAndView;	
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(
				@ModelAttribute("estoque") Estoque estoque, 
				BindingResult result,
				RedirectAttributes attr) {
		
		estoqueService.update(estoque);
		Util.setSuccessMessage(attr);
		return "redirect:/estoque";
	}

	@RequestMapping(value = "/destroy", method = RequestMethod.POST)
	public String remove(
			@RequestParam("id") Long id, 
			RedirectAttributes attr) {
		
		estoqueService.remove(id);
		
		Util.setSuccessMessage(attr);
		return "redirect:/estoque";
	}
	
	@RequestMapping(value = "checkout")
	public ModelAndView checkout(@RequestParam(value = "id") Long id) {
		
		ModelAndView modelAndView = new ModelAndView("estoque/checkout");
		
		modelAndView.addObject("estoque", estoqueService.findById(id));
		
		return modelAndView;	
	}
	
	@RequestMapping(value = "/sell", method = RequestMethod.POST)
	public String sell(
			@RequestParam("id") Long id, 
			@RequestParam(value = "quantidade", defaultValue = "1") Integer quantidade,
			RedirectAttributes attr) {
		
		estoqueService.sell(id, quantidade);
		
		Util.setSuccessMessage(attr);
		return "redirect:/estoque";
	}
	
}
