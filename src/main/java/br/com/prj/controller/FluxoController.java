package br.com.prj.controller;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.prj.models.Fluxo;
import br.com.prj.services.FluxoService;
import br.com.prj.util.Util;

@Controller
@RequestMapping("/fluxo")
public class FluxoController {

	@Autowired
	private HttpServletRequest request;
	@Autowired
	private FluxoService fluxoService;

	@RequestMapping
	public ModelAndView list(
			@RequestParam(value = "start", required = false) Calendar start, 
			@RequestParam(value = "end", required = false) Calendar end,
			@RequestParam(defaultValue = "1") Integer page) {

		PageRequest pageRequest = new PageRequest(page - 1, 10);
		
		ModelAndView modelAndView = new ModelAndView("fluxo/index");

		if (start != null || end != null) {
			
			Page<Fluxo> fluxos = fluxoService.obterFluxoPorData(start, end, pageRequest);
			
			if (fluxos != null) {
				modelAndView.addObject("total", fluxoService.somarValoresObterFluxoPorData(start, end));
				modelAndView.addObject("list", fluxos);
			}
			
		} else {
			modelAndView.addObject("list", fluxoService.findAllOrderByDataDesc(pageRequest));
		}

		return modelAndView;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(
			@ModelAttribute("fluxo") Fluxo fluxo, 
			BindingResult result, 
			RedirectAttributes attr) {
		
		fluxoService.update(fluxo);
		Util.setSuccessMessage(attr);
		return "redirect:/fluxo";
	}

	@RequestMapping(value = "/destroy", method = RequestMethod.POST)
	public String remove(
			@RequestParam("id") Long id, 
			RedirectAttributes attr) {
		
		fluxoService.remove(id);
		
		Util.setSuccessMessage(attr);
		return "redirect:/fluxo";
	}

	@RequestMapping(value = { "/new", "/edit" })
	public ModelAndView view(@RequestParam(value = "id", required = false) Long id) {
		ModelAndView modelAndView = new ModelAndView("fluxo/modal");
		
		if(id == null) {
			modelAndView.addObject("fluxo", new Fluxo());
		} else {
			modelAndView.addObject("fluxo", fluxoService.findById(id));
		}
		
		return modelAndView;	
	}
	
}