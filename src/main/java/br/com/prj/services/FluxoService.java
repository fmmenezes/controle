package br.com.prj.services;

import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.prj.models.Fluxo;
import br.com.prj.repositories.FluxoRepository;
import br.com.prj.util.LineChart;

@Service
public class FluxoService {

	@Autowired
	private EntityManager em;
	@Autowired
	private FluxoRepository repo;

	public List<LineChart> getLineCharts() throws IOException {

		List<LineChart> chart = new LinkedList<LineChart>();

		String sql = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("line_charts.sql"));

		@SuppressWarnings("unchecked")
		List<Object[]> items = em.createNativeQuery(sql).getResultList();

		for (Object[] item : items) {
			chart.add(new LineChart(item[0].toString(), item[3].toString(), item[4].toString()));
		}

		return chart;
	}

	public Double somarValoresObterFluxoPorData(Calendar start, Calendar end) {

		if (start != null && end != null) {
			return repo.countByDataBetween(start, end);
		} else if (start != null) {
			return repo.countByDataGreaterThenEqual(start);
		} else if (end != null) {
			return repo.countByDataLessThenEqual(end);
		}

		return 0d;
	}

	public Page<Fluxo> obterFluxoPorData(Calendar start, Calendar end, Pageable pageable) {
		if (start != null && end != null) {
			return repo.findAllByDataBetweenOrderByDataDesc(start, end, pageable);
		} else if (start != null) {
			return repo.findAllByDataGreaterThanEqualOrderByDataDesc(start, pageable);
		} else if (end != null) {
			return repo.findAllByDataLessThanEqualOrderByDataDesc(end, pageable);
		}

		return null;
	}

	public Page<Fluxo> findAllOrderByDataDesc(Pageable pageable) {
		return repo.findAllByOrderByDataDesc(pageable);
	}

	@Transactional
	public void update(Fluxo fluxo) {
		em.merge(fluxo);
		em.flush();
	}

	public Fluxo findById(Long id) {
		return em.find(Fluxo.class, id);
	}

	@Transactional
	public void remove(Long id) {
		Fluxo find = findById(id);
		em.remove(find);
		em.flush();
	}
}