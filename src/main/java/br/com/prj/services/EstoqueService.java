package br.com.prj.services;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.prj.models.Estoque;
import br.com.prj.models.Fluxo;
import br.com.prj.repositories.EstoqueRepository;
import br.com.prj.repositories.FluxoRepository;

@Service
public class EstoqueService {

	@Autowired
	private EstoqueRepository estoqueRepository;
	@Autowired
	private FluxoRepository fluxoRepository;

	public Page<Estoque> findAllByOrderByDescricao(Pageable pageable) {
		return estoqueRepository.findAllByOrderByDescricao(pageable);
	}

	public Page<Estoque> findAllByDescricaoContainingOrderByDescricao(String descricao, Pageable pageable) {
		return estoqueRepository.findAllByDescricaoContainingOrderByDescricao(descricao, pageable);
	}

	public Estoque findById(Long id) {
		return estoqueRepository.findOne(id);
	}

	public Estoque update(Estoque estoque) {
		estoque.setAtualizado(new Date());
		return estoqueRepository.save(estoque);
	}

	public void remove(Long id) {
		estoqueRepository.delete(id);
	}
	
	public void sell(Long id, Integer quantidade) {
		Estoque item = estoqueRepository.findOne(id);
		item.setQuantidade(item.getQuantidade() - quantidade);
		estoqueRepository.save(item);
		
		BigDecimal qtd = new BigDecimal(quantidade);
		
		String descricao = String.format("%s (%d)", item.getDescricao(), quantidade);
		BigDecimal valor = item.getValor().multiply(qtd);
		
		Fluxo fluxo = new Fluxo(descricao, valor);
		
		fluxoRepository.save(fluxo);
	}
}