package br.com.prj.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.prj.models.Estoque;

@Repository
public interface EstoqueRepository extends PagingAndSortingRepository<Estoque, Long> {

	Page<Estoque> findAllByOrderByDescricao(Pageable pageable);

	Page<Estoque> findAllByDescricaoContainingOrderByDescricao(String descricao, Pageable pageable);
	
}
