package br.com.prj.repositories;

import java.util.Calendar;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.prj.models.Fluxo;

@Repository
public interface FluxoRepository extends PagingAndSortingRepository<Fluxo, Long> {

	@Query("SELECT SUM(f.valor) FROM Fluxo f WHERE f.data BETWEEN :p_start AND :p_end")
	Double countByDataBetween(@Param("p_start") Calendar start, @Param("p_end") Calendar end);

	@Query("SELECT SUM(f.valor) FROM Fluxo f WHERE f.data >= :p_start")
	Double countByDataGreaterThenEqual(@Param("p_start") Calendar start);

	@Query("SELECT SUM(f.valor) FROM Fluxo f WHERE f.data <= :p_end")
	Double countByDataLessThenEqual(@Param("p_end") Calendar end);

	Page<Fluxo> findAllByDataBetweenOrderByDataDesc(Calendar start, Calendar end, Pageable pageable);

	Page<Fluxo> findAllByDataGreaterThanEqualOrderByDataDesc(Calendar start, Pageable pageable);

	Page<Fluxo> findAllByDataLessThanEqualOrderByDataDesc(Calendar end, Pageable pageable);

	Page<Fluxo> findAllByOrderByDataDesc(Pageable pageable);

}
