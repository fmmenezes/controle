<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:blank pgTitle="Relat�rios" pgSubTitle="Gr�ficos que ajudam a ter uma vis�o do seu neg�cio">
	
	<jsp:attribute name="body_area">
		
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Fluxo do caixa</h3>
			</div>
			<div class="box-body chart-responsive">
				<div id="line-chart" class="chart" style="height: 300px;"></div>
			</div>
		</div>
		
	</jsp:attribute>
	
	<jsp:attribute name="extraScripts">
	
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
		
		<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
	
		<script type="text/javascript">
				
			$(function(){
				
				function getLineChartData() {
					
		    		var jsonData = $.ajax({
		              url: "lineCharts",
		              dataType: "json",
		              async: false
		            }).responseJSON;
		    		
	    			var rows = []; 
	    		
		    		$.each(jsonData, function(index, item){
		    			rows.push({ 
	   			            y : item.periodo, 
	   			            r : item.receita, 
	   			            d : item.despesa 
		    			});
		    		});
	    			
		    		return rows;
				}
	    		
				var area = Morris.Area({
				  element: 'line-chart',
				  resize : true,
				  data : getLineChartData(),
				  lineColors : ['#3b65fc', '#ff0000'],
				  xkey: 'y',
				  ykeys: ['r', 'd'],
				  labels: ['Receita', 'Despesa'],
				  parseTime : false,
				  preUnits : 'R$ ',
				  behaveLikeLine : true,
				  hideHover: 'auto'
				});		
				
			});
		</script>
	</jsp:attribute>
	
</t:blank>