<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:blank pgTitle="Estoque" pgSubTitle="Controle o que voc� possui no estoque">

	<jsp:attribute name="body_area">
		
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Pequise, adicione ou edite itens</h3>
              <div class="box-tools">
	              <button data-target="#modal" 
	                		data-toggle="modal" 
	                		data-url="<c:url value="/estoque/new" />" 
	                		class="btn btn-primary btn-sm" aria-label="Adicionar">
	                	<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
	                </button>
                </div>
            </div>
            <div class="box-body">
              
              <form:form id="formrange" cssClass="form-inline" servletRelativeAction="/estoque" method="get">
	              <div class="form-group">
				    <label for="start" class="sr-only">Descri��o</label>
				    <input id="start" type="text" class="form-control" name="descricao" placeholder="Descri��o" value="${param.descricao}" />
				  </div>
	              <button type="submit" class="btn btn-primary">Buscar</button>
              </form:form>
              
              <br>
              
              <c:if test="${not empty list.content}">
            		<div class="row">
						
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped table-condensed">
									<thead>
										<tr>
											<th>Descri��o</th>
											<th>Valor</th>
											<th>Em estoque</th>
											<th>�ltima Atualiza��o</th>
											<th width="100">Op��es</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${list.content}" var="estoque">
											<tr>
												<td>${estoque.descricao}</td>
												<td><fmt:formatNumber value="${estoque.valor}" type="currency" maxFractionDigits="2" currencySymbol="R$" /></td>
												<td>${estoque.quantidade}</td>
												<td>
													<fmt:formatDate value="${estoque.atualizado}" pattern="dd/MM/yyyy HH:mm:ss" />
												</td>
												<td width="130">
													<button data-target="#modal" 
															data-toggle="modal" 
															data-url="<c:url value="/estoque/checkout?id=${estoque.id}" />" 
															class="btn btn-primary btn-sm" aria-label="Checkout"> 
														<span class="glyphicon glyphicon-usd " aria-hidden="true"></span>
													</button>
													<button data-target="#modal" 
															data-toggle="modal" 
															data-url="<c:url value="/estoque/edit?id=${estoque.id}" />" 
															class="btn btn-primary btn-sm" aria-label="Editar"> 
														<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
													</button>
													<form:form servletRelativeAction="/estoque/destroy" cssStyle="display: inline;">
														<input type="hidden" name="id" value="${estoque.id}" />
														<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Tem certeza que deseja remover?');">
															<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
														</button>
													</form:form>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
						</div>
					</div>
				 </div>
			  </c:if>
             
             <c:if test="${empty list.content and not empty param.descricao}">
             	<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					Nenhum item para "${param.descricao}"
				</div>
             </c:if>
             
            </div>
            
            <c:if test="${not empty list.content}">
				<div class="box-footer">
				    <nav>
						<ul id="pagination-demo" class="pagination pagination-sm no-margin pull-right"></ul>
					</nav>
				</div>
			</c:if>
            
        </div>
	</jsp:attribute>
	
	<jsp:attribute name="extraScripts">
		
		<script src="${pageContext.request.contextPath}/resources/app/js/jquery.mask.min.js" type="text/javascript"></script>
		
		<c:if test="${not empty list.content}">
		
			<script type="text/javascript">
				
				$('#pagination-demo').twbsPagination({
			        totalPages: '${list.totalPages}',
			        visiblePages: 5,
			        first: '<<',
			        prev : '<',
			        next : '>',
			        last : '>>',
			        href: '?page={{number}}&descricao=${param.descricao}'
			    });
			
			</script>
			
		</c:if>
		
		<script type="text/javascript">
			$(function() {
				
				$formRange = $("#formrange");
				$formRange.bootstrapValidator({
					container: 'tooltip',
					feedbackIcons : {
						required : 'glyphicon glyphicon-asterisk',
						valid : 'glyphicon glyphicon-ok',
						invalid : 'glyphicon glyphicon-remove',
						validating : 'glyphicon glyphicon-refresh'
					}
				});

			});
		</script>
		
	</jsp:attribute>
	
</t:blank>