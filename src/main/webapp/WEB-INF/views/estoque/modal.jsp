<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<h4 class="modal-title" id="myModalLabel">${empty estoque.id ? 'Cadastrar' : 'Editar'}</h4>
</div>
	
<form:form id="fluxoform" commandName="estoque" cssClass="form-horizontal" servletRelativeAction="/estoque/add">
	<div class="modal-body">
	
		<input type="hidden" name="id" value="${estoque.id}">
		
		<div class="form-group">
			<label for="inputDescricao" class="col-lg-3 control-label">Descri��o</label>
			<div class="col-lg-9">
				<input type="text" name="descricao" class="form-control" id="inputDescricao" placeholder="Descri��o" required="required" value="${estoque.descricao}">
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputQtd" class="col-lg-3 control-label">Qtd. em estoque</label>
			<div class="col-lg-9">
				<input type="number" name="quantidade" class="form-control" id="inputQtd" placeholder="Quantidade" required="required" value="${estoque.quantidade}">
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputValor" class="col-lg-3 control-label">Valor unit�rio</label>
			<div class="col-lg-9">
				<input type="text" name="valor" class="form-control" id="inputValor" placeholder="Valor" required="required" value="${estoque.valor}">
			</div>
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button type="submit" class="btn btn-primary">Enviar</button>
    </div>
             
</form:form>

<script src="${pageContext.request.contextPath}/resources/app/js/jquery.maskMoney.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$(function() {
		
		$form = $("#fluxoform");

		$form.bootstrapValidator({
			container: 'tooltip',
			feedbackIcons : {
				required : 'glyphicon glyphicon-asterisk',
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			}
		});

		$("#inputValor").maskMoney({
			prefix : 'R$ ',
			allowNegative : false,
			allowZero : false,
			thousands : '.',
			decimal : ',',
		}).maskMoney('mask').bind("change keypress", function() {
			$form.bootstrapValidator('revalidateField', this.name);
		});

	});
</script>	