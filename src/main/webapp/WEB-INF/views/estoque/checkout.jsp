<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<h4 class="modal-title" id="myModalLabel">Vender ${estoque.descricao}</h4>
</div>
	
<form:form id="fluxoform" commandName="estoque" cssClass="form-horizontal" servletRelativeAction="/estoque/sell">
	<div class="modal-body">
	
		<input type="hidden" name="id" value="${estoque.id}">
		
		<div class="form-group">
			<label for="inputDescricao" class="col-lg-3 control-label">Descri��o</label>
			<div class="col-lg-9">
				 <p class="form-control-static">${estoque.descricao}</p>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputValor" class="col-lg-3 control-label">Valor unit�rio</label>
			<div class="col-lg-9">
				 <p class="form-control-static"><fmt:formatNumber value="${estoque.valor}" type="currency" maxFractionDigits="2" currencySymbol="R$" /></p>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputQtd" class="col-lg-3 control-label">Quantidade</label>
			<div class="col-lg-9">
				<input type="number" name="quantidade" class="form-control" id="inputQtd" placeholder="Quantidade" required="required" value="1">
			</div>
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button type="submit" class="btn btn-primary">Vender</button>
    </div>
             
</form:form>

<script type="text/javascript">
	$(function() {
		
		$form = $("#fluxoform");

		$form.bootstrapValidator({
			container: 'tooltip',
			feedbackIcons : {
				required : 'glyphicon glyphicon-asterisk',
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			}
		});

	});
</script>	