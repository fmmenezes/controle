<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<h4 class="modal-title" id="myModalLabel">${empty fluxo.id ? 'Cadastrar' : 'Editar'}</h4>
</div>
	
<form:form id="fluxoform" commandName="fluxo" cssClass="form-horizontal" servletRelativeAction="/fluxo/add">
	<div class="modal-body">
	
		<input type="hidden" name="id" value="${fluxo.id}">
		
		<div class="form-group">
			<label for="inputData" class="col-lg-2 control-label">Data</label>
			<div class="col-lg-10">
				<c:if test="${not empty fluxo.data}">
					<fmt:formatDate value="${fluxo.data.time}" pattern="dd/MM/yyyy" var="formattedData" />
				</c:if>
				<input type="text" name="data" class="form-control" id="inputData" placeholder="Data" required="required" value="${formattedData}">
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputDescricao" class="col-lg-2 control-label">Descri��o</label>
			<div class="col-lg-10">
				<input type="text" name="descricao" class="form-control" id="inputDescricao" placeholder="Descri��o" required="required" value="${fluxo.descricao}">
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputValor" class="col-lg-2 control-label">Valor</label>
			<div class="col-lg-10">
				<input type="text" name="valor" class="form-control" id="inputValor" placeholder="Valor" required="required" value="${fluxo.valor}">
			</div>
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button type="submit" class="btn btn-primary">Enviar</button>
    </div>
             
</form:form>

<script src="${pageContext.request.contextPath}/resources/app/js/jquery.maskMoney.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$(function() {
		
		$form = $("#fluxoform");

		$form.bootstrapValidator({
			container: 'tooltip',
			feedbackIcons : {
				required : 'glyphicon glyphicon-asterisk',
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				 data: {
	                validators: {
	                    date: {
	                        format: 'DD/MM/YYYY'
	                    }
	                }
		         }
			}
		
		});

		$('#inputData').datepicker({
			autoclose : true,
			todayHighlight : true,
			format : "dd/mm/yyyy",
			language : "pt-BR",
			todayBtn : "linked"
		}).on('changeDate', function(e) {
			$form.bootstrapValidator('revalidateField', e.currentTarget.name);
		}).mask('00/00/0000');


		$("#inputValor").maskMoney({
			prefix : 'R$ ',
			allowNegative : true,
			allowZero : false,
			thousands : '.',
			decimal : ',',
		}).maskMoney('mask').bind("change keypress", function() {
			$form.bootstrapValidator('revalidateField', this.name);
		});

	});
</script>	