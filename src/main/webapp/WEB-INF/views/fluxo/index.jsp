<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:blank pgTitle="Fluxo de Caixa" pgSubTitle="Acompanhe suas receitas e despesas por periodo">

	<jsp:attribute name="body_area">
		
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Receitas e despesas</h3>
              <div class="box-tools">
	              <button data-target="#modal" 
	                		data-toggle="modal" 
	                		data-url="<c:url value="/fluxo/new" />" 
	                		class="btn btn-primary btn-sm" aria-label="Adicionar">
	                	<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
	                </button>
                </div>
            </div>
            <div class="box-body">
              
              <form:form id="formrange" cssClass="form-inline" servletRelativeAction="/fluxo" method="get">
	              
	              <div class="form-group">
				    <label for="start" class="sr-only">De</label>
				    <input id="start" type="text" class="form-control date" name="start" placeholder="De" value="${param.start}" />
				  </div>
	              
	              <div class="form-group">
	              	<label for="end" class="sr-only">At�</label>
	              	<input id="end" type="text" class="form-control date" name="end" placeholder="At�" value="${param.end}" />
	              </div>
	              
	               <button type="submit" class="btn btn-primary">Buscar</button>
	              
              </form:form>
              
              <br>
              
              <c:if test="${not empty list.content}">
            		<div class="row">
						
						<c:if test="${not empty total}">
							<div class="col-md-12">
								<h3>
									Total em caixa para per�odo: 
									<fmt:formatNumber value="${total}" type="currency" maxFractionDigits="2" currencySymbol="R$" />
								</h3>
							</div>
						</c:if>
						
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped table-condensed">
									<thead>
										<tr>
											<th></th>
											<th>Descri��o</th>
											<th>Valor</th>
											<th>Data</th>
											<th>Adicionado em</th>
											<th width="100">Op��es</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${list.content}" var="fluxo">
											<tr>
												<td>
													<img alt="" src="./resources/app/images/${fluxo.valor lt 0 ? 'despesa' : 'receita'}.png">
												</td>
												<td>${fluxo.descricao}</td>
												<td><fmt:formatNumber value="${fluxo.valor}" type="currency" maxFractionDigits="2" currencySymbol="R$" /></td>
												<td>
													<fmt:formatDate value="${fluxo.data.time}" pattern="dd/MM/yyyy" />
												</td>
												<td>
													<fmt:formatDate value="${fluxo.adicionado}" pattern="dd/MM/yyyy HH:mm:ss" />
												</td>
												<td>
													<button data-target="#modal" 
															data-toggle="modal" 
															data-url="<c:url value="/fluxo/edit?id=${fluxo.id}" />" 
															class="btn btn-primary btn-sm" aria-label="Editar"> 
														<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
													</button>
													<form:form servletRelativeAction="/fluxo/destroy" cssStyle="display: inline;">
														<input type="hidden" name="id" value="${fluxo.id}" />
														<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Tem certeza que deseja remover?');">
															<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
														</button>
													</form:form>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
						</div>
					</div>
				 </div>
			  </c:if>
             
            </div>
            
            <c:if test="${not empty list.content}">
				<div class="box-footer">
				    <nav>
						<ul id="pagination-demo" class="pagination pagination-sm no-margin pull-right"></ul>
					</nav>
				</div>
			</c:if>
            
        </div>
	</jsp:attribute>
	
	<jsp:attribute name="extraScripts">
		
		<script src="${pageContext.request.contextPath}/resources/app/js/jquery.mask.min.js" type="text/javascript"></script>
		
		<link href="${pageContext.request.contextPath}/resources/datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
		<script src="${pageContext.request.contextPath}/resources/datepicker/js/bootstrap-datepicker.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/datepicker/locales/bootstrap-datepicker.pt-BR.min.js" charset="UTF-8"></script>
		
		<c:if test="${not empty list.content}">
		
			<script type="text/javascript">
				
				$('#pagination-demo').twbsPagination({
			        totalPages: '${list.totalPages}',
			        visiblePages: 5,
			        first: '<<',
			        prev : '<',
			        next : '>',
			        last : '>>',
			        href: '?page={{number}}&start=${param.start}&end=${param.end}'
			    });
			
			</script>
			
		</c:if>
		
		<script type="text/javascript">
			$(function() {
				
				$formRange = $("#formrange");
				$formRange.bootstrapValidator({
					container: 'tooltip',
					feedbackIcons : {
						required : 'glyphicon glyphicon-asterisk',
						valid : 'glyphicon glyphicon-ok',
						invalid : 'glyphicon glyphicon-remove',
						validating : 'glyphicon glyphicon-refresh'
					},
					fields : {
						 start: {
			                validators: {
			                    date: {
			                        format: 'DD/MM/YYYY'
			                    }
			                }
				         },
				         end: {
			                validators: {
			                    date: {
			                        format: 'DD/MM/YYYY'
			                    }
			                }
				        },
					}
				});
				
				$('.date').datepicker({
					autoclose : true,
					todayHighlight : true,
					format : "dd/mm/yyyy",
					language : "pt-BR",
					todayBtn : "linked"
				}).on('changeDate', function(e) {
					$formRange.bootstrapValidator('revalidateField', e.currentTarget.name);
				}).mask('00/00/0000');

			});
		</script>
		
	</jsp:attribute>
	
</t:blank>